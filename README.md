## Compila_erros_Pilha (NETBEANS)

- Este projeto contém a Analise Sintática utilizando a PILHA
- Ele contempla também:
	- o tratamento de erros
	- a estrutura if-else


## Folder Structure

- Este projeto está no formato de um projeto Java
- Por isso, temos todos os motores (JFlex e JCup) prontos para serem utilizados sem a necessidade de configuração do computador
	- ou seja, temos os .jars na pasta tools e serão chamados automatica pelo `gerarParser.bat`

- É um projeto criado no NETBEANS  
- O conteúdo é o mesmo do outro projeto VSCode


## Use

- Use a vontade para treinar os conceitos apresentados na video aula e sala de aula
- Sugestões:
	- Faça algumas alterações e testes para sentir o funcionamento do especial error
	- Crie novas regras de produção e vá testando. Assim a gente aprende! 	
